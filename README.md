# Sampling-Based Verification of CTMCs with Uncertain Rates (SLURF)

This repository has been moved permanently to [https://github.com/LAVA-LAB/slurf/](https://github.com/LAVA-LAB/slurf/). Please visit this page to access the repository.